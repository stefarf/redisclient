package redisclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/stefarf/iferr"
)

func (wrp *ClientWrapper) SubscribeAndConsume(channels []string, consume func(channel, payload string) error) error {
	var redisChannels []string
	for _, ch := range channels {
		redisChannels = append(redisChannels, fmt.Sprintf("%s:%s", wrp.namespace, ch))
	}

	ps := wrp.cli.Subscribe(ctx, redisChannels...)
	defer func() { iferr.Print(ps.Close()) }()

	for {
		msg, err := ps.ReceiveMessage(ctx)
		if err != nil {
			return err
		}

		err = consume(strings.TrimPrefix(msg.Channel, wrp.namespace+":"), msg.Payload)
		if err != nil {
			return err
		}
	}
}

func (wrp *ClientWrapper) PublishString(channel string, message string) error {
	redisChannel := fmt.Sprintf("%s:%s", wrp.namespace, channel)
	return wrp.cli.Publish(ctx, redisChannel, message).Err()
}

func (wrp *ClientWrapper) PublishMarshal(channel string, message any) error {
	b, err := json.Marshal(message)
	if err != nil {
		return err
	}
	redisChannel := fmt.Sprintf("%s:%s", wrp.namespace, channel)
	return wrp.cli.Publish(ctx, redisChannel, b).Err()
}
