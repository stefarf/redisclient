package redisclient

import (
	"context"

	"github.com/redis/go-redis/v9"
)

var ctx = context.Background()

type ClientWrapper struct {
	cli       *redis.Client
	namespace string
}

func New(redisServer, namespace string) *ClientWrapper {
	return &ClientWrapper{
		cli:       redis.NewClient(&redis.Options{Addr: redisServer}),
		namespace: namespace,
	}
}
