package redisclient

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/stefarf/iferr"
)

func (wrp *ClientWrapper) Del(key string) error {
	redisKey := fmt.Sprintf("%s:%s", wrp.namespace, key)
	return wrp.cli.Del(ctx, redisKey).Err()
}

func (wrp *ClientWrapper) SetMarshal(key string, value any, expiration time.Duration) error {
	b, err := json.Marshal(value)
	iferr.Panic(err)
	redisKey := fmt.Sprintf("%s:%s", wrp.namespace, key)
	return wrp.cli.Set(ctx, redisKey, b, expiration).Err()
}

func (wrp *ClientWrapper) SetString(key string, value string, expiration time.Duration) error {
	redisKey := fmt.Sprintf("%s:%s", wrp.namespace, key)
	return wrp.cli.Set(ctx, redisKey, value, expiration).Err()
}

func (wrp *ClientWrapper) GetUnmarshal(key string, value any) error {
	redisKey := fmt.Sprintf("%s:%s", wrp.namespace, key)
	rst := wrp.cli.Get(ctx, redisKey)
	b, err := rst.Bytes()
	if err != nil {
		return err
	}
	return json.Unmarshal(b, value)
}

func (wrp *ClientWrapper) GetString(key string) (string, error) {
	redisKey := fmt.Sprintf("%s:%s", wrp.namespace, key)
	rst := wrp.cli.Get(ctx, redisKey)
	b, err := rst.Bytes()
	if err != nil {
		return "", err
	}
	return string(b), nil
}
